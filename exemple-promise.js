// getTempCallback = (location, callback) => {
//   callback(undefined, 78);
//   callback('City not found');
// }
//
// getTempCallback('Paris', function (err, temp){
//   if (err) {
//     console.log('error', err);
//   }else {
//     console.log('success', temp);
//   }
// });
//
// getTempPromise = (location) => {
//   return new Promise(function(resolve, reject){
//     setTimeout(function(){
//       resolve(79);
//       reject('City not found');
//     }, 1000);
//   });
// }
//
// getTempPromise('Paris').then(function(temp){
//   console.log('promise success', temp);
// }, function(err){
//   console.log('Promise error', err);
// })


addPromise = (a , b) => {
  return new Promise(function(resolve, reject){
    if (typeof a === 'number' && typeof b === 'number'  ) {
      resolve(a+b);
    }else {
      reject('vous ne respectez pas le type');
    }
  });
};

addPromise(10, 20).then(function(sum){

      console.log('success', sum);

},function(err){
  console.log('erreur de type, need to be number', err);
});

addPromise('10', 20).then(function(sum){
  console.log('succes', sum);
}, function(err){
  console.log('erreur de type hé hé', err);
})
