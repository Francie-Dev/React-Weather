import axios from 'axios';

const URL = `http://api.openweathermap.org/data/2.5/weather?units=imperial&appid=76005f988eeb176eb4de4e70a34eef10`;

module.exports = {
   getTemp: function(location){
    let encodeLocation = encodeURIComponent(location);
    let url = `${URL}&q=${encodeLocation}`;

    return axios.get(url).then(function(res){
      if(res.data.cod && res.data.message){
        throw new Error(res.data.message);
      }else {
        return res.data.main.temp;
      }
    }, function(res){
      throw new Error(res.data.message);
    });
  }
};
