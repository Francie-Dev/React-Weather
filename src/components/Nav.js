import React from 'react';
import {Link, IndexLink} from 'react-router';

class Nav extends React.Component {

   render(){

     return(
       <div>
         <h1>Nav Component</h1>
         <IndexLink to='/' activeClassName='active' activeStyle={{fontWeight: 'bold'}}>Get Weather </IndexLink>
         <Link to='/about' activeClassName='active' activeStyle={{fontWeight: 'bold'}}>About </Link>
         <Link to='/exemples' activeClassName='active' activeStyle={{fontWeight: 'bold'}}>Exemples </Link>
      </div>
     )
   }
  };

export default Nav;
