import React from 'react';

class WeatherMessage extends React.Component {

   render(){

     const temp = this.props.temp;
     const location = this.props.location;

     return(
       <div>
         <p>Il fait {temp} à {location} </p>
       </div>
     )
   }
};

export default WeatherMessage;
