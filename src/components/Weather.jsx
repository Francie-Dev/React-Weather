import React from 'react';
import WeatherForm from './WeatherForm';
import WeatherMessage from './WeatherMessage';
// api
import openWeatherMap from '../api/openWeatherMap';

class Weather extends React.Component {

  constructor(props){
    super(props);
    this.state = { isLoading: false} ;
  }

  handleSearch = (location) => {
    var that = this;
    this.setState({ isLoading: true});
    openWeatherMap.getTemp(location).then(function(temp){
          that.setState({
            location : location,
            temp: temp,
            isLoading: false
           });
    }, function(errorMessage){
          that.setState({ isLoading: false});
          alert(errorMessage);
        });
  }

  renderMessage = () => {
    const {isLoading, temp, location} = this.state;
    if (isLoading) {
      return <h3>Fetching weather...</h3>
    }else if(temp && location) {
      return <WeatherMessage location={location}  temp={temp}/>
    }
  }

   render(){
     return(
       <div>
         <h1>Weather Component</h1>
         <WeatherForm handleSearch={this.handleSearch} />
         {this.renderMessage()}
       </div>
     )
   }
};

export default Weather;
