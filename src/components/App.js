import React from 'react';
import Nav from './Nav';

class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      name: 'Weather',
    }
  }

   render(){
     const title = this.state.name;

     return(
       <div>
         <Nav />
         <h1>{title}</h1>
         {this.props.children}
       </div>
     )
   }
  };

export default App;
