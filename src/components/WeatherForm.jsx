import React from 'react';

class WeatherForm extends React.Component {

  update = (e) => {
    e.preventDefault();
    let location = this.city.value;
    this.props.handleSearch(location);
    this.form.reset();
  }

   render(){
     return(
       <div>
         <form onSubmit={(e) => this.update(e)} ref={input => this.form = input}>
           <input type='text' ref={input => this.city = input} />
           <button>Get Weather</button>
         </form>
       </div>
     )
   }
  };

export default WeatherForm;
