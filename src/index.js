import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Router, IndexRoute, hashHistory} from 'react-router';
// Layoute
import App from './components/App';
// Page Components
import Weather from './components/Weather';
import About from './components/About';
import Exemples from './components/Exemples';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={Weather} />
      <Route path='about' component={About} />
      <Route path='exemples' component={Exemples} />
    </Route>
  </Router>,
  document.getElementById('root')
);
