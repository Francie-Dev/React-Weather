module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname,
    filename: './src/bundle.js'
  },
  resolve: {
    root: __dirname,
    alias: {
      App: '/src/components/App.js',
      Nav: '/src/components/Nav.js',
      Weather:'/src/components/Weather.jsx',
      WeatherForm:'/src/components/WeatherForm.jsx',
      WeatherMessage:'/src/components/WeatherMessage.jsx',
      About:'/src/components/About.jsx',
      Exemples:'/src/components/Exemples.jsx',
      openWeatherMap:'src/api/openWeatherMap.jsx'
    },
    extensions: ['', '.js','.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets:['react', 'es2015','stage-2']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
};
